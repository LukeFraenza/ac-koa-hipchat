[ ![Codeship Status for atlassianlabs/ac-koa-hipchat](https://codeship.io/projects/22bfa080-fbc3-0131-d6f8-5a73486b8860/status)](https://codeship.io/projects/29249)

# What is this?

A [Node.js](http://nodejs.org) and [Koa.js](http://koajs.com)-based library for building [HipChat Connect add-ons](https://www.hipchat.com/docs/apiv2/addons).

While this is still a pre-1.0 release, the API has mostly stabilized.  Future versions may still include backward-incompatible changes, but the risk of that now is relatively low.

# Getting started

To create and install a simple HipChat add-on, please see our [Getting Started](https://bitbucket.org/atlassianlabs/ac-koa-hipchat/wiki/Getting_Started) guide.

# Example Projects

The example illustrated in the [Getting Started](https://bitbucket.org/atlassianlabs/ac-koa-hipchat/wiki/Getting_Started) guide comes from the following example project:

* [Greeter](https://bitbucket.org/atlassianlabs/ac-koa-hipchat-greeter)

See these additional add-ons for more complete examples:

* [Sassy](https://bitbucket.org/atlassianlabs/ac-koa-hipchat-sassy) (demonstrates multiple /commands)
* [Karma](https://bitbucket.org/atlassianlabs/ac-koa-hipchat-karma) (demonstrates group based data storage independent of installation type)
* [Hearsay](https://bitbucket.org/atlassianlabs/ac-koa-hipchat-hearsay) (demonstrates a configuration UI)
* [Mailroom](https://bitbucket.org/atlassianlabs/ac-koa-hipchat-mailroom) (demonstrates a complex third party webhook integration)
* [Untappd](https://bitbucket.org/atlassianlabs/ac-koa-hipchat-untappd) (demonstrates integrations with multiple third party services)

# Library Features

This library provides help with many aspects of add-on development, such as:

* [Creating an App](https://bitbucket.org/atlassianlabs/ac-koa-hipchat/wiki/Creating_an_App) - including support for creating multiple addons in a single Koa app 
* [Multi-tenant](https://bitbucket.org/atlassianlabs/ac-koa-hipchat/wiki/Multi-tenancy) registration and data partitioning
* High-level conveniences for [mounting webhook handlers](https://bitbucket.org/atlassianlabs/ac-koa-hipchat/wiki/Webhooks)
* Configuration of commonly required Koa middleware
* A [REST API client](https://bitbucket.org/atlassianlabs/ac-koa-hipchat/wiki/REST_Client) with built-in OAuth2 token acquisition and refresh
* JWT authentication validation, refresh, and token generation for web UI routes

In these documentation pages, we use the terms `ctx` and 'Koa context' interchangeably to refer to the context object that contains both standard Koa request/response data and objects and this library's request objects and services.

# FAQ

Frequently asked questions, helpers and tips: [FAQ](https://bitbucket.org/atlassianlabs/ac-koa-hipchat/wiki/FAQ)